package uk.co.zonal.iserve.elastic

import uk.co.zonal.iserve.elastic.client.checkElasticUpAndRunning
import uk.co.zonal.iserve.elastic.entry.grabLogsFromDirectory
import java.io.File
import java.lang.StringBuilder

enum class Command
{
    SCAN,
    PARSE
}

data class ArgumentOptions(val cmd: Command, val directory: File)

fun main(args: Array<String>) {

    parseArguments(args)?.let { options ->

        val shouldPoll = options.cmd == Command.SCAN
        if (shouldPoll && !checkElasticUpAndRunning()) {
            println("Please make sure elasticSearch is installed and running")
            return
        }
        grabLogsFromDirectory(options.directory, shouldPoll)
    }
}

private fun parseArguments(args: Array<String>): ArgumentOptions? = when (args.count()) {
    0,1  -> { println(usage()); null }
    else -> {
        var options: ArgumentOptions? = null

        if (args.count() % 2 == 0) {

            var dir: File?    = null
            var cmd: Command? = null

            args.mapIndexed { i, s -> i to s }.filter { (_,param) -> param.startsWith("-") }.forEach { (index, param) ->
                when (param) {
                    "-d" -> { val obj = File(args[index + 1]); dir = if (obj.exists() && obj.isDirectory && obj.canRead()) obj else null }
                    "-c" -> { val obj = args[index + 1]; cmd = if (obj.equals("scan", true)) Command.SCAN else if (obj.equals("parse", true)) Command.PARSE else null }
                }
            }

            options = dir?.let { d -> cmd?.let { c -> ArgumentOptions(c, d) } }
        }

        if (options == null) {
            println(usage())
        }
        options
    }
}

private fun usage(): String {
    val sb = StringBuilder("\n")
    sb.append("iServe Logs Parser").append("\n\n")
    sb.append("DESCRIPTION").append("\n")
    sb.append("\tThis script scans a directory for iServe logs and crashes. It will push the files' contents to ElasticSearch or \n\tparse them to a human readable form.").append("\n\n")
    sb.append("The Following options are available: ").append("\n")
    sb.append("\t-c\t\tCommand to execute on provided directory. [SCAN: Periodically scan and upload the logs to a running instance of ElasticSearch - PARSER: Convert the logs to human legible form ]").append("\n")
    sb.append("\t-d\t\tThe path to the crash and log files.").append("\n")
    sb.append("\t-h\t\tPrints this message").append("\n")
    return sb.toString()
}
