package uk.co.zonal.iserve.elastic.entry.terminal

data class Terminal(val aztecVersion: String,
                    val eposID: String,
                    val terminalID: String,
                    val terminalName: String,
                    val zcfVersion: String)
