package uk.co.zonal.iserve.elastic.entry.log

data class Crash(val id: String,
                 val name: String?,
                 val reason: String?,
                 val content: String,
                 val screenshot: String?,
                 val timestamp: String)
