package uk.co.zonal.iserve.elastic.entry.device

data class Device(val deviceID: String,
                  val deviceModel: String,
                  val deviceName: String,
                  val deviceOS: String,
                  val deviceType: String)
