package uk.co.zonal.iserve.elastic.entry.log

@Suppress("unused")
enum class LogLevel(val level: String) {
    VERBOSE("VERBOSE"),
    DEBUG("DEBUG"),
    INFO("INFO"),
    WARN("WARN"),
    ERROR("ERROR"),
    FATAL("FATAL"),
    UNKNOWN("UNKNOWN");
}

data class Log(val file: String,
               val level: LogLevel,
               val line: Int,
               val message: String,
               val method: String,
               val thread: String,
               val threadID: String,
               val timestamp: String)
