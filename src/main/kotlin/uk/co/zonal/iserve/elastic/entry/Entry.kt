package uk.co.zonal.iserve.elastic.entry

import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.runBlocking
import org.boon.core.reflection.MapperSimple
import org.boon.json.JsonFactory
import uk.co.zonal.iserve.elastic.client.pushLogs
import uk.co.zonal.iserve.elastic.entry.log.Log
import uk.co.zonal.iserve.elastic.entry.device.Device
import uk.co.zonal.iserve.elastic.entry.log.Crash
import uk.co.zonal.iserve.elastic.entry.terminal.Terminal
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

abstract class LogEntry {
    abstract val id: String
    abstract val log: Log?
}

class Entry(override val id: String,
            override val log: Log?,
            val areaID: String?,
            val areaName: String?,
            val crash: String?,
            val screenshot: String?,
            val siteID: String?,
            val siteName: String?,
            val userID: String?,
            val username: String?,
            val terminal: Terminal?,
            val device: Device) : LogEntry()

class EntryV2(override val id: String,
              override val log: Log?,
              val version: String,
              val areaID: String?,
              val areaName: String?,
              val siteID: String?,
              val siteName: String?,
              val userID: String?,
              val username: String?,
              val terminal: Terminal?,
              val crash: Crash?,
              val device: Device) : LogEntry()

fun grabLogsFromDirectory(dir : File, shouldPollDirectory: Boolean) {
    if (shouldPollDirectory) {
        runBlocking {
            while (true) {
                grabEntriesAndPushToElasticSearch(dir)
                delay(2, TimeUnit.MINUTES)
            }
        }
    } else {
        parseEntries(dir)
    }
}

private val json by lazy { JsonFactory.create() }

@Suppress("UNCHECKED_CAST")
fun payloadFromJSON(file: File): List<LogEntry> {

    val entries = mutableListOf<LogEntry>()
    val line    = file.readText().replace("\uFEFF", "")

    fun createEntry(map: Map<String, Any>) {
        val mapper  = MapperSimple()
        val version = map["version"] as String?
        if (version != null) {
            val entry = mapper.fromMap(map, EntryV2::class.java)
            entries.add(entry)
        } else {
            val entry = mapper.fromMap(map, Entry::class.java)
            entries.add(entry)
        }
    }

    try {
        if (file.name.contains("Crash")) {
            val map = json.fromJson(line) as Map<String, Any>
            createEntry(map)
        } else {

            val newLine = if (line.endsWith("]")) line else line + "]"
            (JsonFactory.create().fromJson(newLine) as List<Map<String, Any>>).forEach { it: Map<String, Any> ->
                createEntry(it)
            }
        }
    } catch (ex: Exception) { }

    return entries
}

private fun String.toDate(): Date {

    fun getFormatter(date: String): SimpleDateFormat = if (date.matches("\\d{2}[-/]\\d{2}[-/]\\d{4} \\d{2}:\\d{2}:\\d{2}.\\d{3}".toRegex())) {
            if (date.contains("-")) SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSSZ")
            else                    SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSSZ")
        } else {
            SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ")
    }

    return getFormatter(this).parse(this)
}

private fun parseEntries(dir: File) {

    println("Scanning ${dir.absolutePath}")
    dir.listFiles { f: File -> f.name.matches(logFilesRegexConstant) }.forEach { file ->

        val entries = payloadFromJSON(file)

        if (!entries.isEmpty()) {
            if (file.name.contains("Crash"))
            {
                val filename = dir.absolutePath + File.separator + file.nameWithoutExtension

                println("parsing crash file: $filename")

                val entry               = entries[0]
                var content: String?    = null
                var screenshot: String? = null

                when (entry) {
                    is Entry   -> { content = entry.crash; screenshot = entry.screenshot }
                    is EntryV2 -> { content = entry.crash?.content; screenshot = entry.crash?.screenshot }
                }

                content?.let { crash ->
                    screenshot?.let { s ->
                        val image = File(filename + ".png")
                        image.writeBytes(Base64.getDecoder().decode(s))
                    }

                    val crashFile = File(filename + ".crash")
                    crashFile.writeText(crash)
                }
            }
            else
            {
                println("parsing log file: ${file.absolutePath}")

                val newEntries = entries.distinctBy { it.id }.map { it }.sortedBy { it.log?.timestamp?.toDate() }
                val sb         = StringBuilder()

                newEntries.forEach { e ->
                    e.log?.let { log ->
                        sb.append(log.level).append(" ")
                                .append(log.timestamp).append(" ")
                                .append("[${log.threadID} - ${log.thread}] ")
                                .append("(${log.file} ${log.method}:${log.line}) ")
                                .append(log.message).append("\n")
                    }
                }

                val logFile = "${dir.absolutePath}${File.separator}_parsed_${file.nameWithoutExtension}.log"
                File(logFile).writeText(sb.toString())
            }
        }
    }
}

private fun grabEntriesAndPushToElasticSearch(dir: File) {

    dir.listFiles { f: File -> f.name.matches(logFilesRegexConstant) }.forEach {
        val entries = payloadFromJSON(it)
        if (!entries.isEmpty()) {

            println("found ${entries.count()} log entries in file: ${it.name}")

            val response = pushLogs(entries)
            if (response.count() == entries.count()) {
                println("pushed all logs to Elastic search... will now delete file")
                it.delete()
            }
        }
    }
}

val logFilesRegexConstant: Regex = "iServe_(Log|Crash)_.*".toRegex()
