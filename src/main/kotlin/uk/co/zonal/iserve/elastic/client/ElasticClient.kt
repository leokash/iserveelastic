package uk.co.zonal.iserve.elastic.client

import org.apache.http.HttpHost
import org.apache.http.entity.ContentType
import org.apache.http.nio.entity.NStringEntity
import org.boon.json.JsonFactory
import org.elasticsearch.client.RestClient
import uk.co.zonal.iserve.elastic.entry.Entry
import uk.co.zonal.iserve.elastic.entry.EntryV2
import uk.co.zonal.iserve.elastic.entry.LogEntry

fun checkElasticUpAndRunning(): Boolean {
    return try {
        RestClient.builder( HttpHost("localhost", 9200,"http")).build().use {
            val response = it.performRequest("GET", "")
            return response.statusLine.statusCode == 200
        }
    } catch (ex: Exception) {
        false
    }
}

private val json by lazy { JsonFactory.create() }

@Suppress("UNCHECKED_CAST")
fun pushLogs(payload: List<LogEntry>): List<String> {

    val result   = mutableListOf<String>()
    val grouping = { e: LogEntry ->
        when (e) {
            is Entry   -> "v1"
            is EntryV2 -> "v2"
            else       -> ""
        }
    }

    payload.groupBy { grouping (it) }.filter { (k,_) -> k != "" }.forEach { key, entries ->

        var index       = 0
        var jsonPayload = StringBuilder()

        entries.forEach { e ->
            jsonPayload.append("{\"index\": {\"_id\": \"${e.id}\"}}").append("\n")
            jsonPayload.append(json.toJson(e)).append("\n")

            if (index > 50_000) {
                result += pushBatch(jsonPayload.toString(), key)
                jsonPayload = StringBuilder()
                index       = 0
            }

            index++
        }

        result += pushBatch(jsonPayload.toString(), key)
    }

    return result
}

private val bulkContentType by lazy { ContentType.create("application/x-ndjson") }

@Suppress("UNCHECKED_CAST")
private fun pushBatch(payload: String, version: String): List<String> {

    val result = mutableListOf<String>()

    RestClient.builder( HttpHost("localhost", 9200,"http")).build().use { client ->

        val endpoint = "iserve_logs_$version/logs/_bulk"
        val response = client.performRequest("PUT", endpoint, mapOf(), NStringEntity(payload, bulkContentType))

        if (response.statusLine.statusCode == 200) {

            val content      = response.entity.content.bufferedReader().use { it.readText() }
            val contentJSON  = json.fromJson(content) as Map<String, Any>
            var successItems = 0

            (contentJSON["items"] as List<Map<String, Any>>).forEach { map: Map<String, Any> ->
                (map["index"] as Map<String, Any>?)?.let {
                    (it["result"] as String?)?.let { res ->
                        when (res) {
                            "created",
                            "updated" -> { val pushedID = (it["_id"] as String); result += pushedID; println("Successfully pushed entry with ID: $pushedID"); successItems += 1 }
                            else      -> { /*Ignore ID as failed to push to Elastic Search*/ }
                        }
                    }
                }
            }

            val millis = contentJSON["took"] as Int
            println("Took ${"%.3f".format(millis.toDouble() / 1000.0)} sec(s) to push $successItems entries\n")
        }
    }

    return result
}
